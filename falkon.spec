%define		major		3

Name:		falkon
Summary:	Cross-platform Qt Web Browser based on QtWebEngine
Version:	3.0.1
Release:	2%{?dist}
Group:		Networking/WWW
License:	GPLv3+ and BSD and LGPLv2+ and GPLv2+ and MPL
URL:		http://www.qupzilla.com/
Source0:	http://download.kde.org/stable/falkon/%{version}/%{name}-%{version}.tar.xz
# reenable native scrollbars by default (upstream disabled them in 2.1.2)
Patch0:		falkon-3.0.0-native-scrollbars.patch
# fix the fix for kde#391300 to require only QtWebEngine 5.10, not Qt 5.10
Patch1:         falkon-3.0.1-mixed-versions.patch
# use the system QtSingleApplication
Patch2:         falkon-3.0.1-system-qtsingleapplication.patch
# implement migration of passwords stored in KWallet or GNOME Keyring
Patch3:         falkon-3.0.1-password-migration.patch

BuildRequires:	kf5-macros
BuildRequires:	pkgconfig(Qt5Concurrent)
BuildRequires:	pkgconfig(Qt5Core)
BuildRequires:	pkgconfig(Qt5DBus)
BuildRequires:	pkgconfig(Qt5Gui)
BuildRequires:	pkgconfig(Qt5Help)
BuildRequires:	pkgconfig(Qt5Network)
BuildRequires:	pkgconfig(Qt5Qml)
BuildRequires:	pkgconfig(Qt5QuickWidgets)
BuildRequires:	pkgconfig(Qt5Script)
BuildRequires:	pkgconfig(Qt5Sql)
BuildRequires:	pkgconfig(Qt5WebChannel)
BuildRequires:	pkgconfig(Qt5WebEngine)
BuildRequires:	pkgconfig(Qt5Widgets)
BuildRequires:	pkgconfig(Qt5X11Extras)
BuildRequires:	qtsingleapplication-qt5-devel
BuildRequires:	qttools5
BuildRequires:	dos2unix
BuildRequires:	pkgconfig(xcb-util)
BuildRequires:	pkgconfig(python3)

Requires:	%{name}-core	= %{version}-%{release}
Requires:	%{name}-plugins	= %{version}-%{release}

Obsoletes:	qupzilla < 2.2.5-2
Provides:	qupzilla = %{version}-%{release}

%description
Falkon is a new KDE web browser, previously known as QupZilla.
Following this release, there will only be one last final QupZilla
release.

Falkon is a new and very fast QtWebEngine browser. It aims to be a lightweight
web browser available through all major platforms. This project has been
originally started only for educational purposes. But from its start, Falkon
has grown into a feature-rich browser.

Falkon has all standard functions you expect from a web browser. It includes
bookmarks, history (both also in sidebar) and tabs. Above that, you can manage
RSS feeds with an included RSS reader, block ads with a builtin AdBlock plugin,
block Flash content with Click2Flash and edit the local CA Certificates
database with an SSL Manager.

Falkon's main aim is to be a very fast and very stable QtWebEngine browser
available to everyone. There are already a lot of QtWebEngine browsers available,
but they are either bound to the KDE environment (rekonq), are not actively
developed or very unstable and miss important features. But there is missing
a multiplatform, modern and actively developed browser. Falkon is trying
to fill this gap by providing a very stable browsing experience.

If you were previously using QupZilla, you can manually migrate
your profiles to Falkon by moving the config directory (usually
in ~/.config/qupzilla/).
There is no automatic migration.

%package core
Summary:	%{name} web browser core package
Group:		Networking/WWW
Provides:	webclient
Requires:	%{_lib}%{name}private%{major} = %{version}-%{release}
Requires:	qtbase5-database-plugin-sqlite
Requires:	qtwebengine5

Obsoletes:	qupzilla-core < 2.2.5-2
Provides:	qupzilla-core = %{version}-%{release}

%description core
Falkon is a new KDE web browser, previously known as QupZilla.
Following this release, there will only be one last final QupZilla
release.

Falkon is a new and very fast QtWebEngine browser. It aims to be a lightweight
web browser available through all major platforms. This project has been
originally started only for educational purposes. But from its start, Falkon
has grown into a feature-rich browser.

Falkon has all standard functions you expect from a web browser. It includes
bookmarks, history (both also in sidebar) and tabs. Above that, you can manage
RSS feeds with an included RSS reader, block ads with a builtin AdBlock plugin,
block Flash content with Click2Flash and edit the local CA Certificates
database with an SSL Manager.

Falkon's main aim is to be a very fast and very stable QtWebEngine browser
available to everyone. There are already a lot of QtWebEngine browsers available,
but they are either bound to the KDE environment (rekonq), are not actively
developed or very unstable and miss important features. But there is missing
a multiplatform, modern and actively developed browser. Falkon is trying
to fill this gap by providing a very stable browsing experience.

If you were previously using QupZilla, you can manually migrate
your profiles to Falkon by moving the config directory (usually
in ~/.config/qupzilla/).
There is no automatic migration.

%package plugins
Summary:	Various plugins for %{name} web browser
Group:		Networking/WWW
Requires:	%{name}-core = %{version}

Obsoletes:	qupzilla-plugins < 2.2.5-2
Provides:	qupzilla-plugins = %{version}-%{release}

%description plugins
Falkon Plugins are dynamically loaded shared libraries (*.so) that can extend
application in almost any way. This package contains the following plugins:

* Mouse Gestures
* Access Keys Navigation
* Personal Information Manager
* GreaseMonkey

%package gnome-keyring
Summary:	GNOME keyring plugin for %{name} web browser
Group:		Networking/WWW
BuildRequires:	pkgconfig(gnome-keyring-1)
Requires:	%{name}-core = %{version}-%{release}

Obsoletes:	qupzilla-gnome-keyring < 2.2.5-2
Provides:	qupzilla-gnome-keyring = %{version}-%{release}

%description gnome-keyring
Plugin for %{name} web browser that allows to store passwords in
GNOME Keyring.

%package kwallet
Summary:	Kwallet plugin for %{name} web browser
Group:		Networking/WWW
BuildRequires:	cmake(KF5Wallet)
Requires:	%{name}-core = %{version}-%{release}

Obsoletes:	qupzilla-kwallet < 2.2.5-2
Provides:	qupzilla-kwallet = %{version}-%{release}

%description kwallet
Plugin for %{name} web browser that allows to store passwords in Kwallet.

%package -n %{_lib}%{name}private%{major}
Summary:	%{name} shared library
Group:		System/Libraries

Obsoletes:	%{_lib}qupzilla2 < 2.2.5-2
Obsoletes:	%{_lib}qupzilla-devel < 2.2.5-2

%description -n %{_lib}%{name}private%{major}
Shared library used by %{name} web browser.

%prep
%setup -q
%autopatch -p1

dos2unix README.md

# unbundle qtsingleapplication
rm -fr src/lib/3rdparty/qtsingleapplication/*
# delete falkon_hellopython translations, we do not package hellopython yet
# note: if you remove this, you also have to add BuildRequires: kf5-ki18n-devel
rm -rf po

%build
%cmake_kf5
%make_build

%install
%make_install -C build

# translations (find_lang_kf5 does not support --all-name, so adapt it)
find %{buildroot}/%{_datadir}/locale/ -name "*.qm" -type f | sed 's:%{buildroot}/::;s:%{_datadir}/locale/\([a-zA-Z_\@]*\)/LC_MESSAGES/\([^/]*\.qm\):%lang(\1) %{_datadir}/locale/\1/LC_MESSAGES/\2:' >%{name}.lang

%files

%files core -f %{name}.lang
%doc CHANGELOG README.md
%license COPYING
%{_bindir}/%{name}
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/themes
%{_iconsdir}/hicolor/*/apps/*.png
%{_iconsdir}/hicolor/*/apps/*.svg
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/applications/org.kde.%{name}.desktop
%{_datadir}/bash-completion/completions/%{name}
%{_datadir}/appdata/org.kde.%{name}.appdata.xml
%dir %{_qt5_plugindir}/%{name}

%files plugins
%{_qt5_plugindir}/%{name}/*.so
%exclude %{_qt5_plugindir}/%{name}/GnomeKeyringPasswords.so
%exclude %{_qt5_plugindir}/%{name}/KWalletPasswords.so

%files gnome-keyring
%{_qt5_plugindir}/%{name}/GnomeKeyringPasswords.so

%files kwallet
%{_qt5_plugindir}/%{name}/KWalletPasswords.so

%files -n %{_lib}%{name}private%{major}
%{_libdir}/libFalkonPrivate.so.%{major}{,.*}

%changelog
* Wed Apr 11 2018 Jeremiah Summers <jsummers@glynlyon.com> 3.0.1-1
- Upgrade to 3.0.1

* Wed Apr 11 2018 Jeremiah Summers <jsummers@glynlyon.com> 3.0.0-3
- new package built with tito
- Rework spec file to be more distro agnostic
